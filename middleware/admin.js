 export default function ({ store, redirect }) {
    if (store.state.auth.user.role_type !== 'admin') {
        return redirect('/login')
    }
}