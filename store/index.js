export const state = () => ({
    roleSwitcher : {
      super_admin: "/dashboard/activitylog",
      doit_admin: "/dashboard/users",
      mdao_admin: "/dashboard/design-studio",
      maker: "/dashboard/cms",
      checker: "/dashboard/cms",
      office_editor: "/dashboard/cms",
      office_checker: "/dashboard/cms",
    },
})

export const mutations = {
    addItem(state, id){
        let item = state.products.find(product => product.id == id)
        state.myRentals.push(item)
    },
}

export const actions = {
}

export const getters = {
  
}