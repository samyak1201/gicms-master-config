export const EDIT_FORM = `editMemberForm`;
export const CREATE_FORM = `createMemberForm`;
export const CLOSE_FORM = `closeMemberForm`;
export const RELOAD_TABLE = `reloadMemberTable`;