import Errors from "~/helpers/error";
console.log(123);
export default {
    data:() => ({
        isLoading: false,
        templates:[],
        update_id:"",
        errors: new Errors()
    }),
     methods: {
        getFileSize(bytes) {
            if (bytes == 0) {
              return "0.00 B";
            }
            var e = Math.floor(Math.log(bytes) / Math.log(1024));
            return (
              (bytes / Math.pow(1024, e)).toFixed(2) + " " + " KMGTP".charAt(e) + "B"
            );
          },
     },
     handleTemplateUpload(event, index, field) {
        const [file] = event.target.files;
        this.templates[index][field] = file;
      },
      openUpdateFileDialog(id) {
        try {
          this.update_id = id;
          this.$refs.updateFile[0].click();
        } catch (error) {
          console.error({ error });
        }
      },
      async updateBuildTemplate($event) {
        try {
          this.$toast.info("uploading template");
          const [file] = $event.target.files;
          const formData = new FormData();
  
          formData.append("_method", "PATCH");
          formData.append("file", file);
  
          const { template } = await this.$axios.$post(
            `/api/templates/${this.update_id}/built-template`,
            formData,
            {
              headers: {
                "Content-Type": "multipart/form-data",
              },
            }
          );
          this.$toast.success("template changed successfully.");
          this.$root.$emit(UPDATE_TEMPLATE, template);
        } catch (error) {
          console.error({ error });
        }
      },
      addNewTemplate() {
        this.templates.push({ vue_file: "", js_file: "" });
      },
      openUpdateBuiltFileDialog(id) {
        this.update_id = id;
        this.$refs.updateBuiltFile[0].click();
      },
      async updateTemplate($event) {
        try {
          this.$toast.info("uploading template");
          const [file] = $event.target.files;
          const formData = new FormData();
  
          formData.append("_method",'PATCH');
          formData.append('file',file);
  
          const { template } = await this.$axios.$post(`/api/templates/${this.update_id}`,
          formData,
          {
          headers: {
                "Content-Type": "multipart/form-data",
              },
          });
          this.$toast.success('template changed successfully.')
          this.$root.$emit(UPDATE_TEMPLATE,template);
        } catch (error) {
          console.error({ error })
          this.$toast.error('Something went wrong.');
        }
      },
}