export const users = {
    'name': "",
    'name_np': "",
    'email': "",
    'institution_id': "",
    "contact_no": "",
}

export const maker = {
    'name': "",
    'name_np': "",
    'email': "",
    'institution_id': "",
    "contact_no": "",
    "site_map_id": [],
    'role': "",
    'password': ""
}

export const role_types = [
    {
        id: 1,
        name: "Super Admin",
        value: "super_admin"
    },
    {
        id: 2,
        name: "Doit Admin",
        value: "doit_admin"
    },
    {
        id: 3,
        name: "Mdao Admin",
        value: "mdao_admin"
    },
    {
        id: 4,
        name: "Maker",
        value: "maker"
    },
    {
        id: 5,
        name: "Checker",
        value: "checker"
    },
];
