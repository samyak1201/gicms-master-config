export const EDIT_FORM = `editDesignationForm`;
export const CREATE_FORM = `createDesignationForm`;
export const CLOSE_FORM = `closeDesignationForm`;
export const RELOAD_TABLE = `reloadDesignationTable`;