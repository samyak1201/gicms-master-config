// actions
export const STORE_WEBSITE_RECORD = `saveWebsiteRecord`;
export const STORE_MODULES = 'storeWebsiteModules';
export const STORE_TEMPLATES = `storeTemplates`;
export const STORE_MODULE_TEMPLATES = `storeModuleTemplates`;
export const SELECT_MODULE = `selectActiveModule`;
export const TOGGLE_SECTION_STATUS = `toggleSectionStatus`;
export const CHANGE_VIEW_PORT = "changeViewPort";
export const TOGGLE_LAYOUT = `toggleTabLayout`;

// mutations 
const SAVE_MODULES = "saveWebsiteModules";
const SAVE_HEADER_FOOTER = 'saveHeaderFooter';



export const state = () => {
    return {
        website: {},
        modules: [],
        sections: {},
        header: {},
        footer: {},
        key: '',
        selected_module: "",
        view_port: "desktop"

    }
}

export const mutations = {
    [SAVE_MODULES](state, modules) {
        state.modules = modules;
    },
    [STORE_MODULES](state, modules) {
        state.modules = modules;
    },
    [SAVE_HEADER_FOOTER](state, [header, footer]) {
        state.header = header;
        state.footer = footer;
    },
    [SELECT_MODULE](state, module_id) {
        state.selected_module = module_id;
    },
    [STORE_MODULE_TEMPLATES](state, sections) {
        state.sections = {
            ...state.sections,
            [state.selected_module]: sections
        }
    },
    [TOGGLE_SECTION_STATUS](state, { section_id, parent_section }) {
        const { sections, selected_module } = state;
        sections[selected_module].map(section => {
            
            if (parent_section && parent_section == section.id) {
                section.childrens.map(child_section => {
                    if (child_section.id == section_id) {
                        const current_status = child_section.is_active;
                        child_section.is_active = !current_status;
                    }
                    return child_section;
                })
            }
            else {
                if (section.id == section_id) {
                    const current_status = section.is_active;
                    section.is_active = !current_status;
                }
            }

            return section;
        })
    },
    [CHANGE_VIEW_PORT](state, port) {
        state.view_port = port;
    },
    [TOGGLE_LAYOUT](state, section_id) {
        const { sections, selected_module } = state;
        sections[selected_module].map(section => {
            if (section.id == section_id) {
                const current_status = section.is_tab_layout;
                section.is_tab_layout = !current_status;
            }
            return section;
        })
    }

}

export const getters = {
    key: (state) => state.key,
    modules: (state) => state.modules,
    sections: state => state.sections,
    selected_module: (state) => state.selected_module,
    module_sections: state => state.sections[state.selected_module] ?? [],
    templates: (state) => {
        if (state.sections[state.selected_module]) {

            const activeSections = state.sections[state.selected_module].filter(section => {
                if (section.is_active) {
                    const records = section.childrens.filter(child_section => child_section.is_active);
                    // if(section.childrens.length > 1) {
                    //     console.table(section.childrens)
                    // }
                    // console.log({records})
                    return {
                        ...section,
                        childrens: records
                    };
                }
            })
            return activeSections;
        }
        return [];
    },
    view_port: state => state.view_port,
    header: (state) => state.header,
    footer: state => state.footer
}

export const actions = {
    [STORE_MODULES]({ commit }, modules) {
        commit(STORE_MODULES, modules);
    },
    [STORE_TEMPLATES]({ commit }, templates) {
        const header = templates.find(template => template.section.name.toLowerCase() === 'header')
        const footer = templates.find(template => template.section.name.toLowerCase() === 'footer')
        commit(SAVE_HEADER_FOOTER, [header, footer])
    },
    [SELECT_MODULE]({ commit }, module_id) {
        commit(SELECT_MODULE, module_id);
    },
    [STORE_MODULE_TEMPLATES]({ commit }, sections) {
        commit(STORE_MODULE_TEMPLATES, sections);
    },
    [TOGGLE_SECTION_STATUS]({ commit }, { section_id, parent_section = null }) {
        commit(TOGGLE_SECTION_STATUS, { section_id, parent_section });
    },
    [CHANGE_VIEW_PORT]({ commit }, port) {
        commit(CHANGE_VIEW_PORT, port);
    },
    [TOGGLE_LAYOUT]({ commit }, section_id) {
        commit(TOGGLE_LAYOUT, section_id);
    }
}

