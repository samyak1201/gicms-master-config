export default function ({ store, redirect, i18n,$toast }) {
    // If the user has not changed the password
    if(!store.state.auth.user.password_changed_at) {
        $toast.info("You need to change your password first before using system.");
        return redirect(`/${i18n.locale}/profile/update`)
    }
  }