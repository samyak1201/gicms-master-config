export const EDIT_FORM = `editGalleryForm`;
export const CREATE_FORM = `createGalleryForm`;
export const CLOSE_FORM = `closeGalleryForm`;
export const RELOAD_TABLE = `reloadGalleryTable`;