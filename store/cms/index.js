// mutations 
export const STORE_SITE_MAPS = `storesitemaps`;

export const state = () => {
    return {
        cmsSiteMaps: []
    }
}

export const mutations = {
    [STORE_SITE_MAPS](state, maps) {
        state.cmsSiteMaps = maps;
    }
}
