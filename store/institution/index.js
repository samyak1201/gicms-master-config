export const STORE_INSTITUTION_TYPE = `storeInstitutionType`;

export const state = () => {
    return {
        institutionType: []
    }
}


export const mutations = {
    [STORE_INSTITUTION_TYPE](state, institutionType) {
        state.institutionType = institutionType;
    }
}

export const actions = {
    [STORE_INSTITUTION_TYPE](context, institutionType) {
        context.commit(STORE_INSTITUTION_TYPE, institutionType);
    }
}

export const getters = {

}