export default function ({ store, redirect }) {
   const { role_type } = store.state.auth.user;
   const switcher = store.state.roleSwitcher;
   
   if (role_type !== 'super_admin') {
      return redirect(switcher[role_type]);
   }
}