// State Specify that for each roles activites log can viewed.

export const state = () => ({
    super_admin: ['super_admin','doit_admin','mdao_admin','maker','checker'],
    doit_admin: ['doit_admin', 'mdao_admin', 'maker', 'checker'],
    mdao_admin: ['mdao_admin', 'maker', 'checker']
})

export const mutations = {
   
}

export const actions = {
}

export const getters = {

}