export const EDIT_FORM = `editSettingForm`;
export const CREATE_FORM = `createSettingForm`;
export const CLOSE_FORM = `closeSettingForm`;
export const RELOAD_TABLE = `reloadSettingTable`;