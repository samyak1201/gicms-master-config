export const EDIT_FORM = `editAccountForm`;
export const CREATE_FORM = `createAccountForm`;
export const CLOSE_FORM = `closeAccountForm`;
export const RELOAD_TABLE = `reloadAccountTable`;
