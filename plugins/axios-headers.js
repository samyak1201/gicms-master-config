export default function ({ $axios,i18n,$store, env }) {
    $axios.defaults.headers = {
        ...$axios.defaults.headers,
        "X-WEBSITE-KEY": env.WEBSITE_KEY,
        "X-LOCALIZATION":i18n.locale
    }
  }