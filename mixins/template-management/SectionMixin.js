import { 
    RELOAD_SECTIONS,
    LOAD_TEMPLATES,
    REMOVE_UPLOADED_TEMPLATE,
    SET_SECTION,
    UPDATE_TEMPLATE
 } from "~/constants/MasterTemplate/event";

export default {
    data: () => ({
        sections: [],
        loaded_sections: {},
        current_module: '',
        isModuleLoading: false,
        uploaded_templates:[],
        selected_section:""
    }),
    created() {
        this.$root.$on(RELOAD_SECTIONS,(current_module) => {
            this.loadSections(current_module);
        });
        this.$root.$on(LOAD_TEMPLATES,(section) => {
            this.loadTemplates(section)
        })
        this.$root.$on(REMOVE_UPLOADED_TEMPLATE,(template_id) => {
            this.removeTemplate(template_id)
        })
        this.$root.$on(UPDATE_TEMPLATE, (template_record) => {
            this.updateTemplateRecord(template_record);
        });

    },
    methods: {
        async loadSections(module_id) {
            try {
                if(module_id) {
                    this.current_module = module_id;
                    // if(!this.loaded_sections[module_id]) {
                        this.isModuleLoading = true;
                        const { module: { sections } } = await this.$axios.$get(`api/master-templates/modules/${module_id}`);
                        this.loaded_sections = {
                            ...this.loaded_sections,
                            [module_id]: sections
                        }
                        this.sections = sections;
                        this.$root.$emit(SET_SECTION,sections);
                    // }
                    // else {
                    //     this.sections = this.loaded_sections[module_id]
                    // }
                    this.isModuleLoading = false;
                }
            } catch (error) {

            }
        },

        async loadTemplates(section_id) {
          try {
              const {templates } = await this.$axios.$get(`api/sections/${section_id}`);
              this.uploaded_templates = templates;
          } catch (error) {
              
          }  
        },
        removeTemplate(template_id) {
            this.uploaded_templates = this.uploaded_templates.filter(template => template.id !== template_id);
            this.loadSections(this.current_module);

        },
        updateTemplateRecord(template_record) {
            const records = this.uploaded_templates.map(template => {
                if(template_record.id == template.id) {
                    template = template_record
                }
                return template
            })
            this.uploaded_templates = records;

        }
    }
};
