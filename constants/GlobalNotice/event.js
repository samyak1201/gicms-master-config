export const EDIT_FORM = `editGlobalNoticeForm`;
export const CREATE_FORM = `createGlobalNoticeForm`;
export const CLOSE_FORM = `closeGlobalNoticeForm`;
export const RELOAD_TABLE = `reloadGlobalNoticeForm`;