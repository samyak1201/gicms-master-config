export const EDIT_FORM = `editUserForm`;
export const CREATE_FORM = `createUserForm`;
export const CLOSE_FORM = `closeUserForm`;
export const RELOAD_TABLE = `reloadUserTable`;