// actions
export const STORE_MODULES = `storeModules`;
export const TOGGLE_MODULE = `toggleModule`;
export const SET_MODULE_SELECT_STATUS = `setModuleStatus`;
export const HANDLE_SEARCH = `handleSearch`;
// mutations
export const CHANGE_MODULE_STATUS = 'changeModuleStatus';
export const FILTER_MODULES = 'filterModules';
export const state = () => ({
    all_modules: [],
    can_update_modules: false,
    search:""
})

export const mutations = {
    [STORE_MODULES](state,modules) {
        state.all_modules = modules;
        state.modules = modules;
    },
    [SET_MODULE_SELECT_STATUS](state,status) {
        state.can_update_modules = status;
    },

    [CHANGE_MODULE_STATUS](state,module_id) {
        if(state.can_update_modules) {

            const modules = state.modules.filter(module => {
                if(module_id === module.id && !module.is_pre_selected) {
                    const status = !module.is_selected;
                    module.is_selected = status;
                }
                return module;
            })
    
            state.modules = modules;
        }
    },
    [FILTER_MODULES](state,search) {
        state.search = search;
    },
}

export const actions = {
    [STORE_MODULES](context,modules) {
        context.commit(STORE_MODULES,modules)
    },
    [TOGGLE_MODULE](context,module_id) {
        context.commit(CHANGE_MODULE_STATUS,module_id);
    },
    [SET_MODULE_SELECT_STATUS](context,status) {
        context.commit(SET_MODULE_SELECT_STATUS,status);
    },
    [HANDLE_SEARCH](context,search_value) {
        context.commit(FILTER_MODULES,search_value)
    }
}

export const getters ={
    modules:(state) => {
        let modules = state.all_modules;
        const search = state.search;
        if(state.search) {
            modules = modules.filter(module => module.name.includes(search))
        }
        return modules;
    },
    search: state => state.search,
}