export const institution = {
    id: "",
    institution_type_id: '',
    name: "",
    abbreviation: "",
    contact_number: "",
    email: "",
    contact_person_name: "",
    contact_person_name_np: "",
    contact_person_email: "",
    contact_person_phone: "",
    status: true
}

