import Authorization from "~/helpers/Authorization";

export default function ({ store, route,error }) {

    const user = new Authorization(store.state.auth.user.role_type);
    const { permission } = route.meta[0];
    if (!user.has(permission)) {
        return error({
            statusCode: 403,
            message: `Sorry! you don't access to ${permission.replace('_',' ')}`
        })
    }
}