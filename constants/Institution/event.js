export const EDIT_FORM = `editInstituteForm`;
export const CREATE_FORM = `createInstituteForm`;
export const CLOSE_FORM = `closeInstituteForm`;
export const RELOAD_TABLE = `reloadInstituteTable`;