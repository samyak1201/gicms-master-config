require('dotenv').config()

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "client",
    htmlAttrs: {
      lang: "en"
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" }
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/assets/scss/main.scss',
    '~/assets/app.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: "~/plugins/paginate", ssr: false },
    { src: "~/plugins/vue-select", ssr: false },
    { src: "~/plugins/axios-error" },
    { src: "~/plugins/axios-headers" },
    { src: "~/plugins/vue-draggable", ssr: false },
    { src: "~/plugins/bootstrap", mode: "client" },
    { src: "~/plugins/vue-tooltip", ssr: false },
    { src: "~/plugins/vue-trix", ssr: false },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/dotenv',
  ],
  publicRuntimeConfig: {
    apiUrl: process.env.API_BASE_URL || 'http://127.0.0.1:8000',
    dsUrl: process.env.DS_BASE_URL || 'http://127.0.0.1:4000'
  },
  mode: 'spa',
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    "@nuxtjs/axios",
    "@nuxtjs/auth-next",
    "@nuxtjs/style-resources",
    "@nuxtjs/toast",
    ["@nuxtjs/proxy", {
      target: process.env.API_BASE_URL || 'http://127.0.0.1:8000',
      pathRewrite: { "^/api/": "/api/" }
    }],
    ['@nuxtjs/i18n', {

      // lazy: true
    }],
    // "~/modules/bootstrap.js"
  ],
  i18n: {
    strategy: "prefix_and_default",
    langDir: "~/translations",
    locales: [
      {
        code: "en",
        name: "EN",
        file: "en.json"
      },
      {
        code: "np",
        name: "NP",
        file: "np.json"
      }
    ],
    defaultLocale: 'en',
    vueI18n: {
      fallbackLocale: 'en',
    },
    lazy: true

  },
  toast: {
    position: 'top-right',
    duration: "5000",
    theme: "bubble",
    register: [ // Register custom toasts
      {
        name: 'my-error',
        message: 'Oops...Something went wrong',
        options: {
          type: 'error'
        }
      }
    ]
  },
  // css configuration
  styleResources: {
    scss: [
      '~/assets/assets/scss/main.scss',
    ],
    css: [
      "~/assets/assets/icon",
    ]
  },
  // css configuration end

  // axios configuration
  axios: {
    baseURL: process.env.API_BASE_URL || 'http://localhost:3000',
    credentials: true,
    proxy: true
  },
  // axios end
  // auth configuration 
  auth: {
    strategies: {
      laravelSanctum: {
        provider: "laravel/sanctum",
        url: process.env.API_BASE_URL || 'http://localhost:3000',
        endpoints: {
          login: { url: "/api/login" },
          logout: { url: '/api/logout', method: 'post' },
        }
      }
    },
    redirect: {
      login: "/login",
      logout: "/login",
      callback: "/login",
      home: "/home"
    }
  },
  // auth configuration end

  // nuxt proxy configuration
  proxy: {
    "/api/": {
      target: process.env.API_BASE_URL || 'http://localhost:8000',
      pathRewrite: { "^/api/": "/api/" }
    }
  },
  // nuxt proxy configuration end
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extractCSS: true,
  },
  // script: [
  //   {
  //     src: "/bootstrap.bundle.min.js"
  //   }
  // ],
};
