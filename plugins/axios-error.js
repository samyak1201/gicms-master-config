export default function ({ $axios, redirect,$toast, i18n, error: nuxtErrorHandler }) {
  $axios.onRequest(config => {
    console.log('Making request to ' + config.url)
  })

  $axios.onError(error => {
    const statusCode = parseInt(error.response && error.response.status)
    let message = "Internal Server Error";

    if (error.response) {
      console.log(error.response.data)
      if (error.response.data.message) {
        message = error.response.data.message;
      }
    }

    if (statusCode == 401) {
      return redirect(`/${i18n.locale}/login`);
    }

    if ([403, 500].includes(statusCode)) {
      return nuxtErrorHandler({
        statusCode,
        message
      })
    }
  })
}