import { account, users } from "@/constants/Account";

const accountClone = Object.assign({}, account);
const usersClone = Object.assign({}, users);

export default {
    props: {
        institutionType: {
            type: Array,
        },
    },
    data() {
        return {
            admin_users: [],
            institution_users: {},
        }
    },
    watch: {
        'account.institution_id': function (val) {
            if(!this['is_edit']) {
                this.account.assigned_user_id = '';
            }
            this.fetchUsers(val);
        }
    },
    methods: {

        async fetchUsers(institution) {

            if (this.institution_users[institution]) {
                this.admin_users = this.institution_users[institution];
            }
            else {
                const users = await this.$axios.$get('/api/users/institutions', {
                    params: {
                        institution
                    }
                });

                this.institution_users = {
                    ...this.institution_users,
                    [institution]: users
                }
                this.admin_users = users;
            }
        }
    },
}