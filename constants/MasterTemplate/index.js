export const master_template = {
    name: "",
    name_np: "",
    description: "",
    description_np: "",
    institution_type_id: "",
    modules: [],
    sections: []
}

export const modules = {
    name: "",
    name_np: "",
    description: "",
    thumbnail: [],
    filePreview: "",
    module_list: [],
    child_module_list: [],
    grand_child_module_list: [],
    site_map_id: ""
}