class Authorization {

    #role_permissions = {
        'super_admin':[
            'template_management',
            'institution_type_management', // => master-template
            'activity_logs',
        ],
        'doit_admin': [
            'website_list_view', // => sites preview
            'global_notification',
            'institution_management',
            'user_management',
            'account_management',
            'template_settings',
            'activity_logs',
            'website_analytics',
            'site_settings'
        ],
        'mdao_admin': [
            'design_studio',
            'activity_logs',
            'content_management',
            'cms_user_management',
            'cms_designation_management',
            'cms_members_management',
            'cms_gallery_management'
        ],
        'checker': [
            'content_management'
        ],
        'maker': [
            'module_management'
        ]
    }

    /**
     * 
     * @param {String} role 
     */
    constructor(role) {
        this.role = role;
    }

    /**
     * 
     * @param {string} permission 
     * @returns {boolean}
     */
    has(permission = '') {
        
        if(!this.role || !permission ) {
            return false;
        }
        
        return !!this.#role_permissions[this.role].includes(permission);
    }

    can(permission = '') {
        return this.has(permission);
    }
}

export default Authorization;
