export const EDIT_FORM = `editGalleryImagesForm`;
export const CREATE_FORM = `createGalleryImagesForm`;
export const CLOSE_FORM = `closeGalleryImagesForm`;
export const RELOAD_TABLE = `reloadGalleryImagesTable`;