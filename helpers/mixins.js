import { Modal } from "bootstrap";
export default {
    data: () => ({
        theModal: ''
    }),
    props: {
        onClose: {
            type: Function,
            default: () => {
                return () => { }
            }
        },
        onOpen: {
            type: Function,
            default: () => {
                return () => { }
            }
        }
    },
    methods: {
        customModal(modalName) {
            const modal = new Modal(document.getElementById(modalName));
            this.theModal = modal;
            document.getElementById(modalName).addEventListener('hidden.bs.modal', this.onClose)
            document.getElementById(modalName).addEventListener('shown.bs.modal', this.onOpen)
        },
        openModal(data) {
            this.customModal(data)
            this.theModal.show();
        },
        closeModal() {
            this.theModal.hide();
        },
    }
}
